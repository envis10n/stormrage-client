var k3y = ["vc2c7q","5c7e1r","cmppiq","4jitu5","uphlaw","xwz7ja","vthf6e","tvfkyq","6hh8xw","sa23uw"];

class l0cket {
    constructor(){
        const self = this;
        this.title = 'l0cket';
        this.unlocked = false;
        this.i = 0;
        this.obj = {l0cket: k3y[0]};
    }
    run(retval){
        const self = this;
        if(self.unlocked)
        {
            return self.obj;
        }
        else if(retval.includes("LOCK_UNLOCKED` l0cket"))
        {
            self.unlocked = true;
            return self.obj;
        }
        if(retval.includes("LOCK_ERROR"))
        {
            if(retval.includes("not the correct security k3y"))
            {
                self.i += 1;
                if(self.i >= k3y.length)
                {
                    return;
                }
                self.obj.l0cket = k3y[self.i];
                return self.obj;
            }
            else
            {
                return self.obj;
            }
        }
    };
}

module.exports = l0cket;