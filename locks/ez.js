const ez21 = [
    "unlock",
    "open",
    "release"
];

const ez40 = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97];

class ez_21 {
    constructor(){
        const self = this;
        this.title = "EZ_21";
        this.unlocked = false;
        this.i = 0;
        this.obj = {};
    };
    run(retval){
        const self = this;
        if(self.unlocked)
        {
            return self.obj;
        }
        else if(retval.includes("LOCK_UNLOCKED` ez_21"))
        {
            self.unlocked = true;
            return self.obj;
        }
        else if(retval.includes("LOCK_ERROR") || retval.includes("unlock command"))
        {
            self.obj = {ez_21: ez21[self.i]};
            self.i += 1;
            return self.obj;
        }
    };
}

class ez_35 {
    constructor(){
        const self = this;
        this.title = "EZ_35";
        this.unlocked = false;
        this.name = false;
        this.i = 0;
        this.digit = 0;
        this.obj = {ez_35: ez21[self.i]};
    };
    run(retval){
        const self = this;
        if(self.unlocked)
        {
            return self.obj;
        }
        else if(retval.includes("LOCK_UNLOCKED` ez_35"))
        {
            self.unlocked = true;
            return self.obj;
        }
        if(retval.includes("LOCK_ERROR"))
        {
            if(retval.includes('digit'))
            {
                self.obj.digit = self.digit;
                self.digit += 1;
                return self.obj;
            }
            else if(retval.includes("unlock command"))
            {
                self.obj = {ez_35: ez21[self.i]};
                self.i += 1;
                return self.obj;
            }
            else
            {
                self.i += 1;
                return self.obj;
            }
        }
    };
}

class ez_40 {
    constructor(){
        const self = this;
        this.title = "EZ_40";
        this.unlocked = false;
        this.name = false;
        this.i = 0;
        this.digit = 0;
        this.obj = {ez_40: ez21[self.i]};
    };
    run(retval){
        const self = this;
        if(self.unlocked)
        {
            return self.obj;
        }
        else if(retval.includes("LOCK_UNLOCKED` ez_40"))
        {
            self.unlocked = true;
            return self.obj;
        }
        if(retval.includes("LOCK_ERROR"))
        {
            if(retval.includes('prime'))
            {
                self.obj.ez_prime = ez40[self.digit];
                self.digit += 1;
                return self.obj;
            }
            else if(retval.includes("unlock command"))
            {
                self.obj = {ez_40: ez21[self.i]};
                self.i += 1;
                return self.obj;
            }
            else
            {
                self.i += 1;
                return self.obj;
            }
        }
    };
}

module.exports.ez_35 = ez_35;
module.exports.ez_21 = ez_21;
module.exports.ez_40 = ez_40;