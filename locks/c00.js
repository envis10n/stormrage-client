const colors = [
    "red",
    "purple",
    "blue",
    "cyan",
    "green",
    "lime",
    "yellow",
    "orange"
];

const cdata = {
    "red":{
        digit: 3,
        complement: "green",
        triad1: "cyan",
        triad2: "lime"
    },
    "purple":{
        digit: 6,
        complement: "lime",
        triad1: "green",
        triad2: "yellow"
    },
    "blue":{
        digit: 4,
        complement: "yellow",
        triad1: "lime",
        triad2: "orange"
    },
    "cyan":{
        digit: 4,
        complement: "orange",
        triad1: "yellow",
        triad2: "red"
    },
    "green":{
        digit: 5,
        complement: "red",
        triad1: "orange",
        triad2: "purple"
    },
    "lime":{
        digit: 4,
        complement: "purple",
        triad1: "red",
        triad2: "blue"
    },
    "yellow":{
        digit: 6,
        complement: "blue",
        triad1: "purple",
        triad2: "cyan"
    },
    "orange":{
        digit: 6,
        complement: "cyan",
        triad1: "blue",
        triad2: "green"
    }
};

class c001 {
    constructor(){
        const self = this;
        this.title = "c001";
        this.unlocked = false;
        this.i = 0;
        this.color = 'red';
        this.obj = {c001: "red"};
    }
    run(retval){
        const self = this;
        if(self.unlocked)
        {
            return self.obj;
        }
        else if(retval.includes("LOCK_UNLOCKED` c001"))
        {
            self.unlocked = true;
            return self.obj;
        }
        if(retval.includes("LOCK_ERROR"))
        {
            if(retval.includes('color_digit') || retval.includes('color digit checksum'))
            {
                self.obj.color_digit = cdata[self.color].digit;
                return self.obj;
            }
            else if(retval.includes("not the correct color name"))
            {
                self.i += 1;
                self.color = colors[self.i];
                self.obj.c001 = self.color;
                return self.obj;
            }
            else
            {
                return self.obj;
            }
        }
    };
}

class c002 {
    constructor(){
        const self = this;
        this.title = "c002";
        this.unlocked = false;
        this.i = 0;
        this.color = 'red';
        this.obj = {c002: "red"};
    }
    run(retval){
        const self = this;
        if(self.unlocked)
        {
            return self.obj;
        }
        else if(retval.includes("LOCK_UNLOCKED` c002"))
        {
            self.unlocked = true;
            return self.obj;
        }
        if(retval.includes("LOCK_ERROR"))
        {
            if(retval.includes('c002_complement'))
            {
                self.obj.c002_complement = cdata[self.color].complement;
                return self.obj;
            }
            else if(retval.includes("not the correct color name"))
            {
                self.i += 1;
                self.color = colors[self.i];
                self.obj.c002 = self.color;
                return self.obj;
            }
            else
            {
                return self.obj;
            }
        }
    };
}

class c003 {
    constructor(){
        const self = this;
        this.title = "c003";
        this.unlocked = false;
        this.i = 0;
        this.color = 'red';
        this.obj = {c003: "red"};
    }
    run(retval){
        const self = this;
        if(self.unlocked)
        {
            return self.obj;
        }
        else if(retval.includes("LOCK_UNLOCKED` c003"))
        {
            self.unlocked = true;
            return self.obj;
        }
        if(retval.includes("LOCK_ERROR"))
        {
            if(retval.includes('c003_triad'))
            {
                self.obj.c003_triad_1 = cdata[self.color].triad1;
                self.obj.c003_triad_2 = cdata[self.color].triad2;
                return self.obj;
            }
            else if(retval.includes("not the correct color name"))
            {
                self.i += 1;
                self.color = colors[self.i];
                self.obj.c003 = self.color;
                return self.obj;
            }
            else
            {
                return self.obj;
            }
        }
    };
}

module.exports.c001 = c001;
module.exports.c002 = c002;
module.exports.c003 = c003;