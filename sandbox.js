const vm = require('vm');

const start = Date.now();
const end = Date.now()+5000;

const lib = function(){
    return {
        security_level_names: ["NULLSEC", "LOWSEC", "MIDSEC", "HIGHSEC", "FULLSEC"],
        get_security_level_name: function(seclevel){
            if(isNaN(seclevel)) return null;
            var levels = ["NULLSEC", "LOWSEC", "MIDSEC", "HIGHSEC", "FULLSEC"];
            return levels[seclevel] ? levels[seclevel]:null;
        },
        corruption_characters: "¡¢Á¤Ã¦§¨©ª",
        rand_int:function(min, max){
            return Math.round(Math.random() * (max - min) + min);
        },
        corrupt: function(text, complexity = 1, rset = "¡¢Á¤Ã¦§¨©ª"){
            var set = rset.split('').sort(function(){
                return 0.5 - Math.random();
            });
            var tarr = text.split(' ');
            tarr.forEach(function(word, i){
                var carr = word.split('');
                tarr[i] = carr.map(function(el){
                    return (Math.random() > complexity*0.05) ? el : set[Math.round(Math.random() * ((set.length-1)-0)+0)];
                }).join('');
            });
            return tarr.join(' ');
        },
        can_continue_execution: function(){
            return (Date.now() < end-100);
        }
    }
}

const siv = function(cb, delay){
    if(typeof(cb) != 'function')
    {
        throw new Error('Callback must be a function.');
    }
    else if(isNaN(delay))
    {
        throw new Error('Delay must be a number.');
    }
    else
    {
        delay = Number(delay);
        var st = Date.now();
        while(lib().can_continue_execution())
        {
            if(Date.now() >= st)
            {
                cb();
                st = Date.now()+delay;
            }
        }
    }
}

const sto = function(cb, delay){
    if(typeof(cb) != 'function')
    {
        throw new Error('Callback must be a function.');
    }
    else if(isNaN(delay))
    {
        throw new Error('Delay must be a number.');
    }
    else
    {
        delay = Number(delay);
        var st = Date.now()+delay;
        while(lib().can_continue_execution())
        {
            if(Date.now() >= st)
            {
                cb();
                break;
            }
        }
    }
}

process.on('message', function(data){
    script = data.script;
    caller = data.caller;
    id = data.id;
    try{
        script = `
            const runstart = Date.now()
            class Error {
                constructor(message = ""){
                    this.message = message;
                    this.stack = "Error: "+this.message;
                }
            }
            console.log = function(...args){
                var temp = args[0];
                if(args.length > 1)
                {
                    if(typeof(temp) == "string")
                    {
                        if(new RegExp(/{\\d}/, 'gi').test(temp))
                        {
                            args.slice(1).forEach(function(arg, i){
                                if(typeof(arg) == "object") arg = JSON.stringify(arg);
                                temp = temp.replace('{'+i+'}', arg);
                            });
                        }
                        else temp = args.join(" ");
                    }
                }
                result.push("Log: "+temp);
            }
            function code(context, args){
`+script+`
            };
            var ret = code(context);
            if(ret != undefined && ret != null)
            {
                if(typeof(ret) == "object")
                {
                    ret = JSON.stringify(ret, null, '\\t');
                }
                result.push("\\n"+ret);
            }
            else
            {
                result.push("\\n"+(ret === null?"null":"undefined"));
            }
            result.join('\\n');`;
        var cscript = new vm.Script(script, {filename:"<anon>", timeout:5000, lineOffset: -27});
        process.send({ok:true, type:"start"});
        var ret = cscript.runInNewContext({scripts: {lib: lib}, setInterval: siv, setTimeout: sto, context:{caller: caller, id: id}, eval:undefined,console:{}, this:undefined, result: []});
        if(ret != undefined || ret != null)
        {
            process.send({ok:true, type:"success", retval: ret});
        }
        else
            process.send({ok:true, type:"success", retval: "Script eval complete."});
        process.exit(0);
    } catch(e){
        try {
            if(typeof(e) == "string")
            {
                process.send({ok: false, type:"error", retval: "Error: "+e});
            }
            else if(e.message.includes("Script execution timed out"))
            {
                process.send({ok:false, type:"error", retval: e.stack.split('\n')[0]});
            }
            else
            {
                process.send({ok:false, type:"error", retval: e.stack.split('\n').slice(0, 5).join('\n')});
            }
        } catch (e) {
            process.send({ok:false, type:"error", retval: "Error: Unhandled exception."});
        }
        process.exit(1);
    }
});