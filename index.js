var proxy = require('./proxy');
var bot = require('./discord/index');
const writer = require('./writer');
const fs = require('fs');
if(!fs.existsSync('./scripts'))
{
    fs.mkdirSync('./scripts');
}
else
{
    fs.readdirSync('./scripts').forEach(function(file){
        fs.rmdirSync('./scripts/'+file);
    });
}
writer.start();

var cuser = "";

bot.farming = false;
bot.farmrun = false;
bot.contell = 0;

bot.on('s_farm', function(){
    bot.farming = true;
    bot.channels.get('443610685092528128').send('Farming operation started.');
});

proxy.on('sys.status', function(dobj){
    if(dobj.data.breach)
    {
        bot.emit('p_breach', cuser, true);
    }
    else if(dobj.data.breach === false)
    {
        bot.emit('p_nominal', cuser);
    }
});

proxy.on('chats.join', function(dobj){
    console.log('[JOIN]::',dobj);
    if(dobj.data.user != "stormrage" && dobj.data.channel == "null_hypothesis")
    {
        writer.write('chats.send {channel:"null_hypothesis", msg:"Welcome to null_hypothesis, @'+dobj.data.user+'."}');
    }
});

proxy.on('chats.leave', function(dobj){
    console.log("[LEAVE]::",dobj);
});

proxy.on('chats.send', function(dobj){
    console.log('[CHAT]::',dobj);
});

proxy.on('chats.tell', function(dobj){
    if(dobj.data.from_user)
    {
        var reg = new RegExp('CON_TELL trigger from: '+dobj.data.from_user);
        if(reg.test(dobj.data.msg))
        {
            bot.emit('p_con_tell', cuser, dobj);
        }
    }
});

proxy.on('incoming', function(dobj){
    console.log("[INCOMING]::", dobj);
    if(dobj.available != undefined && dobj.available == false)
    {
        console.log("Hackmud open and ready for login...");
        setTimeout(function(){
            writer.write('stormrage', false);
        }, 3000);
    }
    else if(dobj.available != undefined && dobj.available == true) {
        bot.emit('p_login', cuser);
    }
});

proxy.on('outgoing', function(dobj){
    if(dobj.data || dobj.script_name)
    {
        console.log("[OUTGOING]::", dobj);
    }
    else
    {
        cuser = dobj.username;
    }
});