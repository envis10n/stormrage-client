var writer = require('../writer');
var proxy = require('../proxy');
const hstring = "012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789";

module.exports = function(cb, test = false){
    writer.write('kernel.hardline', false, false);
    console.log('[HARDLINE]::Waiting for network...');
    proxy.once('kernel.hardline_init', function(dobj){
        console.log('[HARDLINE]::Obtaining address...');
        setTimeout(function(){
            console.log('[HARDLINE]::Forcing connection...');
            writer.write(hstring, false, false, 0);
            proxy.once('kernel.hardline_complete', function(dobj){
                console.log('[HARDLINE]::Hardline active.');
                cb(dobj);
                if(test)
                {
                    console.log('[HARDLINE]::Test complete. Disconnecting...');
                    writer.write('kernel.hardline {dc:true}', false, false, 0);
                }
            });
        }, 12000);
    });
}