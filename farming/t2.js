var proxy = require('../proxy');
var writer = require('../writer');
var hardlined = require('./hardline');
const EventHandler = require('events');
var events = new EventHandler();
var bot;
module.exports = events;

module.exports.set = function(client){
    bot = client;
}

var hardlineto;
var locs = [];
var lock = false;
var hardline = false;
var hltod = false;
var message;
module.exports.working = false;

setInterval(()=>{
    if(locs.length > 0 && !lock && hardline && module.exports.working)
    {
        lock = true;
        var loc = locs.shift();
        writer.writeWait('h0bot.t2_crack {t:#s.'+loc+'}', function(dobj){
            if(!dobj || !dobj.data || !dobj.data.retval)
            {
                lock = false;
                return;
            }
            var ret = dobj.data.retval;
            try {
                bot.guilds.first().channels.get('444909131338612737').send('Data from `h0bot.t2_crack {t:#s.'+loc+'}`\n\n```js\n'+JSON.stringify(ret, null, '\t')+'\n```');
            }catch(e){
                console.log(e);
                bot.guilds.first().channels.get('444909131338612737').send(e.message);
            };
            lock = false;
        }, 12, true);
    }
    else if((locs.length == 0 || hltod) && module.exports.working)
    {
        hltod = false;
        if(hardlineto) clearTimeout(hardlineto);
        module.exports.working = false;
        if(hardline) writer.write('kernel.hardline {dc:true}');
        hardline = false;
        message.reply('Farming operation completed.');
        writer.writeWait('jade.vita', function(dobj){
            if(!dobj) return;
            var balance = new RegExp(/Account balance == ([A-Z0-9]+GC)/).exec(dobj.data.retval)[1];
            if(balance != "0GC")
            {
                writer.writeWait('jade.vita {withdraw:"'+balance+'", instant:true}', function(dobj){
                    message.reply(balance);
                    message = undefined;
                });
            }
        });
        lock = false;
    }
    else if(!module.exports.working)
    {
        locs = [];
        lock = false;
        if(hardline) writer.write('kernel.hardline {dc:true}');
        hardline = false;
    }
}, 100);

events.on('t2_farm', (msg, corp)=>{
    message = msg;
    writer.writeWait('h0bot.t2_scrape {t:#s.'+corp+'}', function(dobj){
        if(dobj && dobj.data && dobj.data.retval && dobj.data.retval.length)
        {
            if(typeof(dobj.data.retval) == "string")
            {
                msg.reply(dobj.data.retval);
                return;
            }
            module.exports.working = true;
            locs = dobj.data.retval[1];
            msg.reply('Scraped '+locs.length+' '+(locs.length > 1?'locs':'loc')+'.\nPrepping and entering hardline...');
            hardlined(function(dobj){
                hardline = true;
                msg.reply('Hardline active. Farming operation started.');
                hardlineto = setTimeout(function(){
                    hltod = true;
                },(dobj.data.retval.remaining-2)*1000);
            });
        }
        else console.log('[FARM_T2]::Error scraping T2 corp.');
    }, 12, true);
});