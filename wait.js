var proxy = require('./proxy');
module.exports = function(script_name, cb){
    proxy.once(script_name, function(dobj){
        cb(dobj);
    });
}