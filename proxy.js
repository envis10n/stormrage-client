const net = require('tls');
const https = require('https');
const fs = require('fs');
const express = require('express');
var proxy = require('express-http-proxy');
const EventHandler = require('events');
const bParse = require('body-parser');

var events = new EventHandler();

module.exports = events;

var ssloptions = {
    pfx: fs.readFileSync('./_certificates/hackmud.pfx'),
    passphrase: "1234"
};

function incoming(dobj){
    if(dobj instanceof Array)
    {
        dobj.forEach(obj=>{
            if(obj.data) obj.data = JSON.parse(obj.data);
            if(obj.script_name)
            {
                switch(obj.script_name)
                {
                    case 'kernel.hardline':
                        if(obj.data.retval.ok && obj.data.retval.remaining) module.exports.emit(obj.script_name+'_complete', obj);
                        else module.exports.emit(obj.script_name+'_init', obj);
                    break;
                    default:
                        module.exports.emit(obj.script_name, obj);
                    break;
                }
            }
            else module.exports.emit('incoming', obj);
            console.log('::[INCOMING]::\n',obj);
        });
    }
    else if(typeof(dobj) == 'object')
    {
        if(dobj.data) dobj.data = JSON.parse(dobj.data);
        if(dobj.script_name) module.exports.emit(dobj.script_name, dobj);
        else module.exports.emit('incoming', dobj);
        console.log('::[INCOMING]::\n', dobj);
    }
}

function outgoing(dobj){
    console.log('::[OUTGOING]::\n', dobj);
    module.exports.emit('outgoing', dobj);
}

var app = express();
app.use(bParse.json());
app.use(proxy('165.227.249.235', {
    https: true, 
    preserveHostHdr: true,
    userResDecorator: function(proxyRes, proxyResData, userReq, userRes) {
        incoming(JSON.parse(proxyResData.toString('utf8')))
        return proxyResData;
    },
    proxyReqBodyDecorator: function(bodyContent, srcReq) {
        outgoing(bodyContent);
        return bodyContent;
    }
}));
var server = https.createServer(ssloptions, app);

server.listen(443, ()=>{
    console.log('Proxy online.');
});