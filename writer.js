const child_process = require('child_process');
const waitForScript = require('./wait');

var window = "";
var last = "";
var queue = [];
var busy = false;
var hold = false;
var wasOpen = true;

function uniq(a) {
    var prims = {"boolean":{}, "number":{}, "string":{}}, objs = [];

    return a.filter(function(item) {
        var type = typeof item;
        if(type in prims)
            return prims[type].hasOwnProperty(item) ? false : (prims[type][item] = true);
        else
            return objs.indexOf(item) >= 0 ? false : objs.push(item);
    });
}

function setLast(msg){
    last = msg.toLowerCase();
    setTimeout(function(){
        last = "";
    }, 5000);
}

module.exports.clearQueue = function(){
    queue = [];
    busy = false;
    hold = false;
}

module.exports.start = function(){
    child_process.exec('xdotool search "hackmud"', function(err, stdout, stderr){
        if(err)
        {
            wasOpen = false;
            child_process.execSync('LD_LIBRARY_PATH=~/.steam/bin32/ ~/.steam/bin32/steam-runtime/run.sh ~/.steam/steam/steamapps/common/hackmud/hackmud_lin.x86_64');
            console.log("Hackmud not open. Waiting 1500 ms then trying again...");
            setTimeout(module.exports.start, 1500);
            return;
        }
        window = stdout.slice(0,stdout.length-1);
        console.log("Hackmud windowID: "+window);
        setInterval(function(){
            if(queue.length > 0 && !busy && !hold)
            {
                queue = uniq(queue);
                var obj = queue.shift();
                if(obj)
                {
                    if(obj.command)
                    {
                        let message = obj.command;
                        let wait = obj.wait;
                        let delay = obj.delay;
                        if(!delay) delay = 12;
                        busy = true;
                        var command = `xdotool type --delay `+delay+` --window `+window+` '`+message+`\n'`;
                        child_process.execSync(command);
                        if(!wait)
                        {
                            busy = false;
                            return;
                        }
                        else
                        {
                            console.log('Waiting for script: ',message.split(' ')[0]);
                            if(!obj.itimeout)
                            {
                                var stout = setTimeout(function(){
                                    if(obj.cb) obj.cb();
                                    busy = false;
                                }, 6500);
                            }
                            waitForScript(message.split(' ')[0], function(ret){
                                if(!obj.itimeout)
                                    clearTimeout(stout);
                                if(obj.cb) obj.cb(ret);
                                busy = false;
                            });
                        }
                    }
                }
            }
        }, 100);
        if(wasOpen == true)
        {
            module.exports.write('sys.status');   
        }
    });
}

module.exports.write = function(message, wait = true, override = false, delay = 12){
    if(window == "") return;
    if(!override)
    {
        if(message.toLowerCase() != last)
        {
            setLast(message);
            queue.push({command: message, wait: wait, delay: delay});
        }
    }
    else
    {
        hold = true;
        var temp = queue.slice();
        queue = new Array(temp.length+1);
        queue[0] = message;
        temp.forEach(function(el){
            queue.push(el);
        });
        child_process.execSync('xdotool key --window '+window+' Escape');
        hold = false;
    }
};

module.exports.writeWait = function(message, cb, delay = 12, ignoreTimeout = false){
    if(window == "") return;
    if(message.toLowerCase() != last)
    {
        setLast(message);
        queue.push({command: message, wait: true, cb: cb, delay: delay, itimeout: ignoreTimeout});
    }
    else
    {
        cb(null);
    }
};