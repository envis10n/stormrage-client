const Discord = require('discord.js');
const config = require('../config.json');
var writer = require('../writer.js');
var client = new Discord.Client();
var proxy = require('../proxy');
var hardline = require('../farming/hardline');
var t2farm = require('../farming/t2');
t2farm.set(client);
const cp = require('child_process');

var waitForScript = require('../wait');

var user_scripts = new Map();

var is_panic = false;
var panicto;

var con_tell = {
    count: 0,
    lt: -1,
    rt: -1
}

var u_colors = {
    "`0":"gray",
    "`1":"white",
    "`2":"green",
    "`3":"blue",
    "`4":"purple",
    "`5":"orange"
}

function uniq(a) {
    var prims = {"boolean":{}, "number":{}, "string":{}}, objs = [];

    return a.filter(function(item) {
        var type = typeof item;
        if(type in prims)
            return prims[type].hasOwnProperty(item) ? false : (prims[type][item] = true);
        else
            return objs.indexOf(item) >= 0 ? false : objs.push(item);
    });
}

client.on('ready', function(){
    console.log("Discord bot online.");
    client.user.setStatus("online");
    client.user.setActivity("hackmud", {type:"PLAYING"});
});

client.login(config.token);

client.on('error', function(err){
    client.login(config.token);
});

function canCommand(member){
    return (member.roles.find("name","commander") != undefined || member.hasPermission("ADMINISTRATOR"));
}

function canEval(message){
    var member = message.member;
    return ((member.roles.find("name","developer") != undefined && message.channel.id == "445591787164139521") || member.hasPermission("ADMINISTRATOR") || member.roles.find("name","commander") != undefined);
}

client.on('p_login', function(user){
    client.channels.get('443610685092528128').send('Logged in as: `'+user+'`.');
});

client.on('p_con_tell', function(user, dobj){
    //TODO: Breach triggering?
    writer.writeWait('sys.access_log {count:1, start:0}', function(dobj2){
        if(dobj2 && dobj2.data && dobj2.data.retval)
        {
            var reg = new RegExp('Breach attempt from '+dobj.data.from_user+'.', 'g');
            if(reg.test(dobj2.data.retval))
            {
                client.channels.get('443610685092528128').send('Received CON_TELL from `'+dobj.data.from_user+'`. Breach attempt confirmed. Running panic...');
                client.emit('p_breach', user, false);
                panicto = setTimeout(function(){
                    client.emit('p_nominal', user);
                }, 60000 * (Math.round(Math.random() * (7 - 4)+4)));
            }
            else
            {
                client.channels.get('443610685092528128').send('Received CON_TELL from `'+dobj.data.from_user+'`. Breach attempt unconfirmed.');
            }
        }
    });
});

client.on('p_breach', function(user, breach){
    if(!is_panic)
    {
        writer.writeWait('jade.vita {deposit:#s.accts.balance, xfer:#s.accts.xfer_gc_to}', function(dobj){
            client.channels.get('443610685092528128').send("<@&316357592135893004> <@&316357958126796800> All funds deposited.");
            writer.writeWait('stormrage.panic', function(dobj){
                client.channels.get('443610685092528128').send("Important upgrades transferred.");
                writer.writeWait('stormrage.salary {lock:true, confirm:true}', function(dobj){
                    client.channels.get('443610685092528128').send("Salary system locked.");
                });
            });
        });
        is_panic = true;
    }
    if(breach)
    {
        if(panicto) clearTimeout(panicto);
        client.channels.get('443610685092528128').send("<@&316357592135893004> <@&316357958126796800> WARNING: User `"+user+"` has been breached.");
    }
});
client.on('p_nominal', function(user, announce = true){
    if(is_panic)
    {
        writer.writeWait('jade.vita', function(dobj){
            var balance = new RegExp(/Account balance == ([A-Z0-9]+GC)/).exec(dobj.data.retval)[1];
            if(balance != "0GC")
            {
                writer.writeWait('jade.vita {withdraw:"'+balance+'", instant:true}', function(dobj){
                    client.channels.get('443610685092528128').send("Funds withdrawn.");
                    writer.writeWait('stormrage.salary {unlock:true, confirm:true}', function(dobj){
                        client.channels.get('443610685092528128').send("Salary system unlocked.");
                        writer.write('user kiljaeden', false);
                        setTimeout(function(){
                            writer.writeWait('kiljaeden.panic', function(dobj){
                                writer.write('user stormrage', false);
                            });
                        }, 5000);
                    });
                });
            }
            else
            {
                writer.writeWait('stormrage.salary {unlock:true, confirm:true}', function(dobj){
                    client.channels.get('443610685092528128').send("Salary system unlocked.");
                    writer.write('user kiljaeden', false);
                    setTimeout(function(){
                        writer.writeWait('kiljaeden.panic', function(dobj){
                            writer.write('user stormrage', false);
                        });
                    }, 5000);
                });
            }
        });
        is_panic = false;
    }
    if(announce)
        client.channels.get('443610685092528128').send("<@&316357592135893004> <@&316357958126796800> Systems nominal.");
});

client.on('guildMemberAdd', function(member){
    client.guilds.first().channels.get('316351114188423168').startTyping();
    setTimeout(function(){
        client.guilds.first().channels.get('316351114188423168').send("<@"+member.id+">\nWelcome, sentience.\nSystem firewall active.\nUnauthorized access will result in swift punishment.\n*Enjoy your stay!*");
        client.guilds.first().channels.get('316351114188423168').stopTyping();
    }, 1500);
    client.guilds.first().channels.get('443610685092528128').send("<@&316357592135893004> <@&316357958126796800> A new user joined the server: `"+member.displayName+"`");
});

/*proxy.on('chats.send', function(dobj){
    if(dobj.data.channel == "0000" && dobj.data.user == "trust")
    {
        client.guilds.first().channels.get('444947871226265648').send(dobj.data.msg);
    }
});*/

client.on("message", function(message){
    try {
        if(message.author.bot) return;
        if(message.channel.type == "dm") return;
        if(message.content[0] == "!" && canCommand(message.member))
        {
            var msg = message.content.substring(1);
            if(msg == "ping")
            {
                message.reply('pong');
            }
            else if(msg.split(' ')[0] == "ban")
            {
                if(message.mentions.members.size > 0)
                {
                    var hammer = message.guild.emojis.find('name','banhammer');
                    message.channel.send("<@"+message.mentions.members.first().id+">, you have been banned. "+hammer.toString());
                }
            }
            else if(msg.split(' ')[0] == "simbreach")
            {
                message.reply('simulating breach and reset...');
                client.emit('p_breach', '', true);
                setTimeout(function(){
                    client.emit('p_nominal', '');
                }, 20000);
            }
            else if(msg.split(' ')[0] == "raw")
            {
                if(msg.split(' ')[1][0] == '#')
                {
                    message.reply("I can't do that.");
                    return;
                }
                if(msg.split(' ')[1] == 'kernel.hardline')
                {
                    message.reply("I can't do that.");
                    return;
                }
                writer.writeWait(msg.split(' ').slice(1).join(' '), (dobj)=>{
                    if(dobj && dobj.data && dobj.data.retval)
                    {
                        var ret = dobj.data.retval;
                        if(typeof(ret) == 'object')
                        {
                            ret = JSON.stringify(ret, null, '\t');
                        }
                        if(ret.length < 1990)
                            message.reply("```js\n"+ret+"\n```");
                        else message.reply('Return was over 2000 characters.');
                    }
                    else
                    {
                        message.reply('done.');
                    }
                });
            }
            else if(msg.split(' ')[0] == "upgrade")
            {
                switch(msg.split(' ')[1]){
                    case 'list':
                        writer.writeWait('sys.upgrades {full:true}', function(dobj){
                            if(!dobj || !dobj.data || !dobj.data.retval)
                            {
                                console.log(dobj);
                                message.reply('an unexpected error occurred.');
                                return;
                            }
                            var terms = msg.split(' ').slice(2);
                            var npad = dobj.data.retval.reduce((a,b)=> a.name.length > b.name.length ? a : b).name.length;
                            var loaded = 0;
                            var list = dobj.data.retval.map(el=>{
                                var rarity = el.rarity;
                                rarity = u_colors[new RegExp(/(`\d)[\s\S]+`/).exec(rarity)[1]];
                                var id = el.i < 10 ? '00'+el.i : el.i < 100 ? '0'+el.i : el.i;
                                var pad = npad - el.name.length;
                                if(el.loaded) loaded += 4;
                                return (el.loaded?'(*) ':'    ')+'['+id+'] '+el.name+' '.repeat(pad)+' T'+el.tier+' ['+rarity+']';
                            });
                            if(terms && terms.length > 0)
                            {
                                var t = [];
                                list.forEach(a=>{
                                    var good = true;
                                    terms.some(term=>{
                                        if(!a.toLowerCase().includes(term.toLowerCase()))
                                        {
                                            good = false;
                                            return true;
                                        }
                                    });
                                    if(good) t.push(a);
                                });
                                list = t;
                            }
                            if(list.length == 0)
                            {
                                message.reply('no upgrades found with your search terms.');
                                return;
                            }
                            var co = list.length;
                            var ca = list.reduce((a,b)=>a.length > b.length ? a : b).length;
                            var cl = 18+loaded+(list.length);
                            var c = Math.ceil(co*ca)/(2000-cl);
                            var a = Math.ceil(co/c);
                            var ct = [];
                            for(var i = 0;i<c;i++)
                            {
                                var h = list.slice(i*a, (i+1)*a);
                                ct.push(h.join('\n'));
                            }
                            ct.forEach((h,i)=>{
                                message.reply('```\nPage '+(i+1)+'/'+ct.length+'\n\n'+h+'\n```');
                            });
                        });
                    break;
                    case 'info':
                        var i = msg.split(' ')[2];
                        if(isNaN(i))
                        {
                            message.reply('you must supply a number value.');
                            return;
                        }
                        i = Number(i);
                        writer.writeWait('sys.upgrades {i:'+i+'}', function(dobj){
                            dobj.data.retval.rarity = dobj.data.retval.rarity.replace(/`\d/gi, '').replace(/`/gi, '');
                            message.reply('```json\nInfo for upgrade id '+(i < 10 ? '00'+i : i < 100 ? '0'+i : i)+'\n\n'+JSON.stringify(dobj.data.retval, null, '\t')+'\n```');
                        });
                    break;
                    case 'cull':
                        var type = 0;
                        var color = msg.split(' ')[2];
                        if(!color)
                        {
                            message.reply('you must specify which upgrades to cull.');
                            return;
                        }
                        if(!isNaN(color))
                        {
                            type = 1;
                            color = '['+msg.split(' ').slice(2).join(',')+']';
                        }
                        switch(type)
                        {
                            case 0:
                                writer.writeWait('sys.upgrades {full:true}', function(dobj){
                                    if(!dobj || !dobj.data || !dobj.data.retval)
                                    {
                                        console.log(dobj);
                                        message.reply('an unexpected error occurred.');
                                        return;
                                    }
                                    var list = [];
                                    dobj.data.retval.forEach(function(u){
                                        if(!u.loaded)
                                        {
                                            var rarity = u_colors[new RegExp(/(`\d)[\s\S]+`/).exec(u.rarity)[1]];
                                            if(rarity == color)
                                            {
                                                list.push(u.i);
                                            }
                                        }
                                    });
                                    var cull = '['+list.join(',')+']';
                                    writer.writeWait('sys.cull {i:'+cull+', confirm:true}', function(dobj){
                                        if(dobj.data.retval.ok == false)
                                        {
                                            message.reply(dobj.data.retval.msg);
                                        }
                                        else
                                        {
                                            message.reply('upgrades culled.');
                                        }
                                    });
                                });
                            break;
                            case 1:
                                writer.writeWait('sys.cull {i:'+color+', confirm:true}', function(dobj){
                                    if(dobj.data.retval.ok == false)
                                    {
                                        message.reply(dobj.data.retval.msg);
                                    }
                                    else
                                    {
                                        message.reply('upgrades culled.');
                                    }
                                });
                            break;
                        }
                    break;
                    case 'xfer':
                        var i = msg.split(' ')[2];
                        if(isNaN(i))
                        {
                            message.reply('you must supply a number value.');
                            return;
                        }
                        i = Number(i);
                        var target = msg.split(' ')[3];
                        writer.writeWait('sys.upgrades {i:'+i+'}', function(dobj){
                            if(dobj.data.retval.loaded == true)
                            {
                                message.reply("can't transfer loaded upgrades.");
                                return;
                            }
                            writer.writeWait('sys.xfer_upgrade_to {to:"'+target+'", i:'+i+'}', function(dobj){
                                if(dobj.data.retval.ok == true)
                                    message.reply('upgrade transferred to `'+target+'`.');
                                else
                                    message.reply('error transferring upgrade.\n```\n'+dobj.data.retval.msg+'\n```');
                            });
                        });
                    break;
                    case 'k3ys':
                        writer.writeWait('envis10n.upgrades {k3ys:true}', function(dobj){
                            if(dobj && dobj.data && dobj.data.retval && dobj.data.retval.reply) dobj.data.retval.reply = undefined;
                            if(dobj) message.reply('```js\n'+JSON.stringify(dobj.data.retval, null, '\t')+'\n```');
                            else message.reply('an error occurred.');
                        });
                    break;
                    case 'load':
                    case 'unload':
                        var loader = msg.split(' ').slice(2).join(' ');
                        var command = '{'+(msg.split(' ')[1] == "load" ? 'load: ' : 'unload: ')+loader+'}';
                        writer.writeWait('sys.manage '+command, function(dobj){
                            if(dobj.data.retval.ok == true)
                                message.reply('upgrade(s) '+(msg.split(' ')[1] == "load" ? 'loaded.':'unloaded.'));
                            else
                                message.reply('error '+(msg.split(' ')[1] == "load" ? 'loading':'unloading')+' upgrade(s).\n```\n'+dobj.data.retval.msg+'\n```');
                        });
                    break;
                    default:
                        message.reply('available sub-commands: \n\nxfer <id> <to> - Transfer an unloaded upgrade to a user.\n\nlist <?search terms> - Returns a list of system upgrades.\n\t\t* Optional: Search terms - Filter the list by the provided terms.\n\ninfo <id> - Display upgrade info for a specific upgrade.');
                    break;
                }
            }
            else if(msg.split(' ')[0] == "xfer_gc_to")
            {
                var target = msg.split(' ')[1];
                var amount = msg.split(' ')[2];
                if(!target)
                {
                    message.reply('You need a target.');
                    return;
                }
                if(!amount)
                {
                    message.reply('You need to provide an amount.');
                    return;
                }
                var command = "accts.xfer_gc_to ";
                if(!isNaN(amount))
                {
                    command += '{to:"'+target+'", amount: '+amount+'}';
                }
                else
                {
                    command += '{to:"'+target+'", amount:"'+amount+'"}';
                }
                writer.writeWait(command, function(dobj){
                    if(dobj.data && dobj.data.retval)
                    {
                        if(dobj.data.retval.ok)
                        {
                            message.reply(amount+' transferred to `'+target+'`.');
                        }
                        else
                        {
                            message.reply('An error occurred.\n```js\n'+dobj.data.retval.msg+'\n```');
                        }
                    }
                });
            }
            else if(msg.split(' ')[0] == "qclear")
            {
                writer.clearQueue();
                message.reply('sent command to release queue locks and holds.');
            } 
            else if(msg.split(' ')[0] == "status")
            {
                writer.writeWait('sys.status', function(dobj){
                    if(dobj)
                        message.reply(dobj.data.retval.breach?'-system breach-':'-system status nominal-');
                });
            }
            else if(msg.split(' ')[0] == "balance")
            {
                writer.writeWait('accts.balance', function(dobj){
                    if(dobj)
                        message.reply(dobj.data.retval);
                });
            }
            else if(msg.split(' ')[0] == "t2_farm")
            {
                if(!t2farm.working)
                {
                    var corp = msg.split(' ')[1];
                    if(!corp) return;
                    message.reply('Scraping '+corp+'...');
                    t2farm.emit('t2_farm', message, corp);
                }
                else
                {
                    message.reply('Currently farming. Come back later.');
                }
            }
        }
        if(message.content[0] == "!")
        {
            var msg = message.content.substring(1);
            if(msg.split(' ')[0] == "party")
            {
                message.reply(':confetti_ball:');
            }
            else if((msg.split(' ')[0] == "eval" || msg.split('\n')[0] == "eval") && canEval(message))
            {
                var uscript = user_scripts.get(message.member.id);
                if(uscript)
                {
                    message.reply("You already have a script running.");
                    return;
                }
                var codereg = new RegExp(/\s?\n?```js\n?\s?([\s\S]*)\n?\s?```\n?/, 'gi');
                var code = codereg.exec(message.content);
                if(code)
                {
                    var script = cp.fork(process.cwd()+'/sandbox.js');
                    user_scripts.set(message.member.id, script);
                    script.send({script:code[1],caller: message.author.username,id:message.author.id});
                    var tout;
                    script.on('message', function(res){
                        if(res.type == "start")
                        {
                            tout = setTimeout(function(){
                                script.kill();
                                message.reply("```js\nError: Script execution timed out.\n```");
                                user_scripts.delete(message.member.id);
                            }, 5000);
                        }
                        else
                        {
                            clearTimeout(tout);
                            if(typeof(res.retval) == "object")
                                res.retval = "json\n"+JSON.stringify(res.retval, null, '\t');
                            else res.retval = "js\n"+res.retval;
                            if(res.retval.toString().length <= 2000-7)
                                message.reply("```"+res.retval+"\n```");
                            else
                                message.reply("The result was greater than 2000 characters.");
                            user_scripts.delete(message.member.id);
                        }
                    });
                }
                else
                {
                    message.reply('you are missing a JS codeblock.');
                }
            }
        }
    } catch(e){
        console.error(e);
    }
});

module.exports = client;

setInterval(function(){
    if(!t2farm.working)
        writer.write('sys.status', false);
}, 60000 * 5);